FROM tomcat:8.5.20-jre8
MAINTAINER "dernan <caidenan@gmail.com>" # 维护者
RUN echo "Asia/shanghai" > /etc/timezone
ADD server.xml /usr/local/tomcat/conf/
CMD ["catalina.sh", "run"] #启动tomcat shell执行程序
